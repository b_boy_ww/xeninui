-- This is purely for static functions for query builder.

class Helpers
  static ASC = "ASC"
  static DESC = "DESC"
  static UPSERT = true
  static NULL = {
    raw = "NULL"
  }
  
  static raw(str)
    return {
      raw = str
    }
  end

  static fixArgument(arg)
    if (!istable(arg)) then
      return Helpers.construct(arg)
    end

    return arg
  end

  static construct(arg)
    return {
      mysql = arg,
      sqlite = arg,
    }
  end

  static duplicate(arg)
    return {
      left = arg,
      right = arg
    }
  end

  static days(amt)
    local days = (60 * 60 * 24) * amt
    
    return {
      mysql = {
        left = days,
        right = `INTERVAL ${amt} DAYS`
      },
      sqlite = Helpers.duplicate(days)
    }
  end

  static timeSubtract(current, subtract)
    current = Helpers.fixArgument(current)
    subtract = Helpers.fixArgument(subtract)
    
    return {
      mysql = `${current.mysql.left} - ${subtract.mysql.right}`,
      sqlite = `${current.sqlite.left} - ${subtract.sqlite.right}`
    }
  end

  static nowDateTime() 
    return {
      raw = true,
      value = "now()"
    }
  end

  static now()
    return {
      mysql = Helpers.duplicate("UNIX_TIMESTAMP(now())"),
      sqlite = Helpers.duplicate("strftime('%s', 'now')")
    }
  end

  static cast(field, alias, length, typeMySQL, typeSQLite = typeMySQL)
    local lengthStr = ""
    if (length) then
      lengthStr = `(${length})`
    end

    local str = `CAST(${field} AS ${typeMySQL}${lengthStr}) AS ${alias}`

    return Helpers.construct(str)
  end

  static castChar(field, alias, length)
    return Helpers.cast(field, alias, length, "CHAR", "CHAR")
  end

  static count(fields, alias)
    local str = `COUNT(${fields}) AS ${alias}`

    return Helpers.construct(str)
  end

  static between(low, high)
    return Helpers.construct(`BETWEEN ${low} AND ${high}`)
  end

  static sid64(ply)
    if (!IsValid(ply)) then return end -- rip

    return Helpers.construct(ply:SteamID64())
  end

  static upsertDifference(tbl)
    return {
      raw = true,
      upsertDifference = true,
      data = tbl
    }
  end

  __type() return "XeninUI.ORM.Helpers" end
end

XeninUI.ORM.Helpers = Helpers